public with sharing class AccountButtonSearch {

        @AuraEnabled
        public static List<Account> getAccountList(String searchKey){
            try {
                return [SELECT Id, Name FROM Account WHERE Name = :searchKey LIMIT 10];
                
            } catch (Exception e) {
                throw new AuraHandledException(e.getMessage());
            }
         }
}