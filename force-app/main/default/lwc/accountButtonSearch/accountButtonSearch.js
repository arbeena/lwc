import { LightningElement,  track } from 'lwc';
import getAccountList from '@salesforce/apex/AccountButtonSearch.getAccountList';

export default class AccountButtonSearch extends LightningElement {
    // @wire(getAccountList)
    // accounts;

    @track accountList; 
    @track searchKey;
   
    get recordResponse(){
       console.log(this.accountList+"  response");
       return this.accountList ? true : false;    
   }

   accountHandler(){
       getAccountList({searchKey : this.searchKey}).
       then(result => {
           this.accountList = result;
       })  
       .catch(error => {
           this.error = error;
       })
           
   }

   onSearchHandler(event){
       this.searchKey = event.target.value;
       console.log(this.searchKey);       
   }

   onSearchButtonHandler(){
       this.accountHandler();
   }



}