import { LightningElement, wire, track } from 'lwc';
 import getAccountList from '@salesforce/apex/AccountTableController.getAccountList';

export default class AccountTable extends LightningElement {

    // @wire(getAccountList)
    // accounts;

     @track accountList; 
     @track searchKey;
    
     get recordResponse(){
        console.log(this.accountList+"  response");
        return this.accountList ? true : false;    
    }

    @wire(getAccountList, {searchKey : "$searchKey"})
    accountRecords({data, error}){
        if(data){
            this.accountList = data;
            console.log(this.accountList+">>>>>>>>");
        }
        if(error){
            console.error(error);
        }
    }
 

    onSearchHandler(event){
        this.searchKey = event.target.value;
        console.log(this.searchKey);       
    }

}