import { LightningElement, track } from 'lwc';
import createAccount from '@salesforce/apex/AccountUIController.createAccount';

export default class AccountUITask extends LightningElement {

    @track objAcc = {};
    @track accountId;

    createAccountHandler(){
        createAccount({objAcc : this.objAcc})
        .then(res=>{
            this.objAcc = res;
        }).catch(error=>{
            console.log(error);
        });
    }

    onChangeHandler(event){
        let fieldName = event.target.name;
        let value = event.target.value;
        this.objAcc[fieldName] = value;
    }

    addressHandler (event)
    {
        let addressName = event.target.name;
        console.log('Address Name: '+addressName );

        if( addressName == "BillingAddress")
        {
            this.objAcc['BillingStreet'] = event.target.street;
            this.objAcc['BillingCity'] = event.target.city;
            this.objAcc['BillingState'] = event.target.province;
            this.objAcc['BillingCountry'] = event.target.country;
            this.objAcc['BillingPostalCode'] = event.target.postalCode;
        }
        else
        {
            this.objAcc['ShippingStreet'] = event.target.street;
            this.objAcc['ShippingCity'] = event.target.city;
            this.objAcc['ShippingState'] = event.target.province;
            this.objAcc['ShippingCountry'] = event.target.country;
            this.objAcc['ShippingPostalCode'] = event.target.postalCode;
        }
        console.log( JSON.parse(JSON.stringify(this.objAcc)));
    }

    createRecordHandler(){
        this.createAccountHandler();
    }
}